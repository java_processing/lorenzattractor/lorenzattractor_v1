# Lorenz Attractor

Lorenz Attractor visualisation.
Explicit Euler method and Runge Kutta method (2nd order, explicit) are implemented.

Sources and documents :
* Daniel Shiffman's video : [# Coding Challenge #12: The Lorenz Attractor in Processing](https://www.youtube.com/watch?v=f0lkz2gSsIk)
* [Lorenz system Wikipedia page](https://en.wikipedia.org/wiki/Lorenz_system)
* [Euler method wikipedia page](https://en.wikipedia.org/wiki/Euler_method)
* [Runge-Kutta methods Wikipedia page](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods)
