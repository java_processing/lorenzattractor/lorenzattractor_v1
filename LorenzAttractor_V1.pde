
import java.util.ArrayList;
import controlP5.*;
import peasy.*;

//I finally learned how to handle conflict

ControlP5 cp5;   // for the slider that controls the color
Slider slider;
final String SLIDER_NAME = "Relative Spacial Period for Color";
float spacialClrFreq;
PeasyCam cam;   // for camera rotation

double x_init = 0.01d;
double y_init = 0d;
double z_init = 0d;
VectDouble xyz = new VectDouble(x_init, y_init, z_init);
final double sigm = 10d;
final double rho = 28d;
final double beta = 8/3d;
final double dt = 1/64000d;

final float frameRt = 1000;
final float scale = 4.5;
boolean remove = false;

ArrayList<PVector> points = new ArrayList<PVector>();

void setup() {
  frameRate(frameRt);
  size(1200, 900, P3D);

  colorMode(HSB, 100, 100, 100);
  noFill();
  strokeWeight(2);

  cp5 = new ControlP5(this);
  slider = cp5.addSlider(SLIDER_NAME).setPosition(0, 0).setRange(0.001, 1.5);
  slider.setDecimalPrecision(3);
  slider.setScrollSensitivity(0.001);
  slider.setValue(1.5);
  slider.setSize(650, 30);
  
  cam = new PeasyCam(this, 0, 0, 0, 400);
  cam.setMinimumDistance(0.5);
  cam.setMaximumDistance(750);
  perspective(PI/3,width/(float)height,0.01,10000); 

  points.add(xyz.clone().toPVector().mult(scale));
}

void draw() {
  background(0);
  
  for (int i=0 ; i<400 ; i++) {
    EulerExpliciteLorenz(xyz);
    //RungeKuttaLorenz(xyz);
  }

  points.add(xyz.clone().toPVector().mult(scale));

  translate(0, 25, -120);
  
  int i = 0;
  push();
  PVector pnt = points.get(i);
  strokeWeight(12);
  stroke(0, 100, 100);
  point(pnt.x, pnt.y, pnt.z);
  pop();
  
  beginShape();
  float modulo = points.size() * slider.getValue();
  for (PVector vect : points) {
    float clr = i % modulo;
    clr = map(clr, 0, modulo, 0, 100);
    stroke(clr, 100, 100);
    vertex(vect.x, vect.y, vect.z);
    i++;
  }
  endShape();
  
  push();
  strokeWeight(12);
  pnt = points.get(i-1);
  point(pnt.x, pnt.y, pnt.z);
  pop();
  
  if (frameCount>2000 && frameRate<250 && remove==false) {
    remove = true;
  }
  if (remove) {
    points.remove(0);
  }
  //println(frameRate);

  cam.beginHUD();
  cp5.draw();
  cp5.setAutoDraw(false);
  cam.endHUD();
}

public VectDouble EulerExpliciteLorenz(VectDouble vect) {
  vect.add(Lorenz(vect).mult(dt));
  return vect;
}

public VectDouble RungeKuttaLorenz(VectDouble vect) {
  VectDouble pred = vect.clone().add(Lorenz(vect).mult(dt/2.));
  vect.add((Lorenz(pred).add(Lorenz(vect))).mult(dt/2.));
  return vect;
}

public VectDouble Lorenz(VectDouble vect) {
  double x, y, z;
  x = sigm * (vect.y - vect.x);
  y = vect.x * (rho - vect.z) - vect.y;
  z = vect.x * vect.y - beta * vect.z;
  return new VectDouble(x, y, z);
}
