class VectDouble implements Cloneable {
  
  double x, y, z;
  
  VectDouble(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
  }  
  
  VectDouble() {
    this.x = 0;
    this.y = 0;
    this.z = 0;
  }  
  
  @Override
  public VectDouble clone() {
    return new VectDouble(this.x, this.y, this.z);
  }
  
  public PVector toPVector() {
    return new PVector((float)this.x, (float)this.y, (float)this.z);
  }
  
  public VectDouble add(VectDouble vect) {
    this.x += vect.x;
    this.y += vect.y;
    this.z += vect.z;
    
    return this;
  }
  
  public VectDouble sub(VectDouble vect) {
    this.x -= vect.x;
    this.y -= vect.y;
    this.z -= vect.z;
    
    return this;
  }
  
  public VectDouble mult(double dbl) {
    this.x *= dbl;
    this.y *= dbl;
    this.z *= dbl;
    
    return this;
  }
}
